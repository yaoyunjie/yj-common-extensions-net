﻿using System;
using System.Linq;
using System.Security;

namespace YJ.DCrmHelper.Net
{
    public class ServiceConfig
    {
        private static string connectionString;
        private string url = null;

        /// <summary>
        /// Constructor that parses a connection string
        /// </summary>
        /// <param name="connectionStringParam">The connection string to instantiate the configuration</param>
        public ServiceConfig(string connectionStringParam)
        {
            connectionString = connectionStringParam;

            Url = GetParameterValue("Url");

            string userName = GetParameterValue("UserName");
            if (!string.IsNullOrEmpty(userName))
            {
                UserName = userName;
            }

            string domain = GetParameterValue("Domain");
            if (!string.IsNullOrEmpty(userName))
            {
                Domain = domain;
            }

            if (Guid.TryParse(GetParameterValue("CallerObjectId"), out Guid callerObjectId))
            {
                CallerObjectId = callerObjectId;
            }

            string versionValue = GetParameterValue("Version");
            if (!string.IsNullOrEmpty(versionValue))
            {
                Version = versionValue;
            }

            if (byte.TryParse(GetParameterValue("MaxRetries"), out byte maxRetries))
            {
                MaxRetries = maxRetries;
            }

            if (ushort.TryParse(GetParameterValue("TimeoutInSeconds"), out ushort timeoutInSeconds))
            {
                TimeoutInSeconds = timeoutInSeconds;
            }


            string pwd = GetParameterValue("Password");
            if (!string.IsNullOrEmpty(pwd))
            {
                var ss = new SecureString();

                pwd.ToCharArray().ToList().ForEach(ss.AppendChar);
                ss.MakeReadOnly();

                Password = ss;
            }


        }

        /// <summary>
        /// The Url to the CDS environment, i.e "https://yourorg.api.crm.dynamics.com"
        /// </summary>
        public string Url
        {
            get => url; set

            {
                if (!string.IsNullOrEmpty(value))
                {
                    url = value;
                }
                else
                {
                    throw new Exception("Service.Url value cannot be null.");
                }
            }
        }


        /// <summary>
        /// The user name of the user. i.e. you@yourorg.onmicrosoft.com
        /// </summary>
        public string UserName { get; set; } = null;

        /// <summary>
        /// The domain of the user. i.e. you@yourorg.onmicrosoft.com
        /// </summary>
        public string Domain { get; set; } = null;

        /// <summary>
        /// The password for the user 
        /// </summary>
        public SecureString Password { get; set; } = null;

        /// <summary>
        /// The Azure AD ObjectId for the user to impersonate other users.
        /// </summary>
        public Guid CallerObjectId { get; set; }
        /// <summary>
        /// The version of the Web API to use
        /// Default is '9.1'
        /// </summary>
        public string Version { get; set; } = "8.2";
        /// <summary>
        /// The maximum number of attempts to retry a request blocked by service protection limits.
        /// Default is 3.
        /// </summary>
        public byte MaxRetries { get; set; } = 3;
        /// <summary>
        /// The amount of time to try completing a request before it will be cancelled.
        /// Default is 120 (2 minutes)
        /// </summary>
        public ushort TimeoutInSeconds { get; set; } = 120;

        /// <summary>
        /// Extracts a parameter value from a connection string
        /// </summary>
        /// <param name="parameter">The name of the parameter value</param>
        /// <returns></returns>
        private static string GetParameterValue(string parameter)
        {
            try
            {
                string value = connectionString
                    .Split(';')
                    .Where(s => s.Trim()
                    .StartsWith(parameter))
                    .FirstOrDefault()
                    .Split('=')[1];
                if (value.ToLower() == "null")
                {
                    return string.Empty;
                }
                return value;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
