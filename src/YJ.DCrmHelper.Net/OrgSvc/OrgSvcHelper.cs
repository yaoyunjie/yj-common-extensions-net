﻿using Microsoft.Xrm.Tooling.Connector;
using System;

namespace YJ.DCrmHelper.Net.OrgSvc
{
    public class OrgSvcHelper
    {
        public static CrmServiceClient Connect()
        {
            CrmServiceClient service;

            //You can specify connection information in cds/App.config to run this sample without the login dialog
            if (string.IsNullOrEmpty(ConnectionString.DCrmConnectionString))
            {
                throw new ArgumentNullException($"DCrmConnectionString:You must define a connectionString in appsetting.json which called DCrmConnectionString");
            }

            // Try to create via connection string. 
            service = new CrmServiceClient(ConnectionString.DCrmConnectionString);

            return service;

        }
    }
}
