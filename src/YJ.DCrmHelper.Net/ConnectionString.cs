﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace YJ.DCrmHelper.Net
{
    public class ConnectionString
    {
        /// <summary>
        /// 配置文件的根节点
        /// </summary>
        private static readonly IConfigurationRoot Config;

        /// <summary>
        /// Constructor
        /// </summary>
        static ConnectionString()
        {
            // 加载appsettings.json，并构建IConfigurationRoot
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true);
            Config = builder.Build();
        }

        /// <summary>
        /// 当前环境
        /// Development|Production
        /// </summary>
        public static string CurrentEnv => Config["ServerInfo:CurrentEnv"];

        /// <summary>
        /// CRM连接字符串
        /// 示例:AuthType=AD;Url=http://ip:port/orgname;Domain=;UserName=administrator;Password=;CallerObjectId=null;Version=8.2;MaxRetries=3;TimeoutInSeconds=180;
        /// </summary>
        public static string DCrmConnectionString => Config[$"ConnectionStrings:{CurrentEnv}DCrmConnectionString"];
    }
}
