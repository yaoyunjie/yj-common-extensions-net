 ### :alien: YJ.DCrmHelper.Net  :alien: 

#### Dynamics CRM,OrganizationService,WEBAPI,OData

#### Dynamics CRM组织服务以及WebAPI使用帮助类


##### 配置项说明
项目的配置项读取的是根目录下的appsettings.json文件，主要读取项为ConnectionStrings节点下的DevelopmentDCrmConnectionString和ProductionDCrmConnectionString，格式如下：
```
{
"ConnectionStrings": {
    "DevelopmentDCrmConnectionString": "AuthType=AD;Url=http://ip:port/orgname;Domain=domain;UserName=username;Password=password;CallerObjectId=null;Version=8.2;MaxRetries=3;TimeoutInSeconds=180;",
    "ProductionDCrmConnectionString": "正式环境连接字符串"
  }
}
```
##### 使用示例
###### OrgSvc
```c#
CrmServiceClient service = null;
try
            {
                service = OrgSvc.OrgSvcHelper.Connect();

                if (service == null) throw new Exception("CrmServiceClient获取失败");
                // Service implements IOrganizationService interface 
                if (service.IsReady)
                {

                    Entity newAccount = new Entity("account");

                    newAccount["name"] = "Fourth Coffee";

                    newAccount["address2_postalcode"] = "98074";

                    Guid accountid = service.Create(newAccount);

                    return;
                }
                else
                {
                    const string UNABLE_TO_LOGIN_ERROR = "Unable to Login to Microsoft Dataverse";
                    if (service.LastCrmError.Equals(UNABLE_TO_LOGIN_ERROR))
                    {
                        Console.WriteLine("Check the connection string values in cds/App.config.");
                        throw new Exception(service.LastCrmError);
                    }
                    else
                    {
                        throw service.LastCrmException;
                    }
                }
            }

            catch (Exception)
            {
                throw;
            }


            finally
            {
                if (service != null)
                    service.Dispose();
            }
```
###### WebAPI
```C#
static readonly ServiceConfig config = new ServiceConfig(ConnectionString.DCrmConnectionString);

using (CDSWebApiService svc = new CDSWebApiService(config)){
	var contact1 = new JObject
                    {
                        { "firstname", "Rafelp" },
                        { "lastname", "Shillol" }
                    };
                    Uri contact1Uri = svc.PostCreate("contacts", contact1);
}
```