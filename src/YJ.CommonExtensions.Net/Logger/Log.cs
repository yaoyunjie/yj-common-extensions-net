﻿using NLog;

namespace YJ.CommonExtensions.Net.Logger
{
    /// <summary>
    /// Log类
    /// </summary>
    public class Log
    {
        private static NLog.Logger _logger = LogManager.GetCurrentClassLogger();

        private Log() { }

        /// <summary>
        /// Trace
        /// </summary>
        /// <param name="strMsg"></param>
        public static void Trace(string strMsg)
        {
            _logger.Trace(strMsg);
        }
        /// <summary>
        /// Debug
        /// </summary>
        /// <param name="strMsg"></param>
        public static void Debug(string strMsg)
        {
            _logger.Debug(strMsg);
        }
        /// <summary>
        /// Info
        /// </summary>
        /// <param name="strMsg"></param>
        public static void Info(string strMsg)
        {
            _logger.Info(strMsg);
        }
        /// <summary>
        /// Warn
        /// </summary>
        /// <param name="strMsg"></param>
        public static void Warn(string strMsg)
        {
            _logger.Warn(strMsg);
        }
        /// <summary>
        /// Error
        /// </summary>
        /// <param name="strMsg"></param>
        public static void Error(string strMsg)
        {
            _logger.Error(strMsg);
        }
        /// <summary>
        /// Fatal
        /// </summary>
        /// <param name="strMsg"></param>
        public static void Fatal(string strMsg)
        {
            _logger.Fatal(strMsg);
        }
    }
}
