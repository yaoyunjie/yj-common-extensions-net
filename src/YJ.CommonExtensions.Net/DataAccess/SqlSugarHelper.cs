﻿using System;
using System.Linq;
using SqlSugar;
using YJ.CommonExtensions.Net.Domain;

namespace YJ.CommonExtensions.Net.DataAccess
{
    /// <summary>
    /// 使用SqlSugar框架对数据库进行操作
    /// </summary>
    public class SqlSugarHelper
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SqlSugarHelper()
        {
            DbSqlSugarClient = new SqlSugarClient(new ConnectionConfig()
            {

                ConnectionString = AppSettings.ConnectionString, //必填, 数据库连接字符串
                DbType = DbType.SqlServer, //必填, 数据库类型
                IsAutoCloseConnection = true, //默认false, 时候知道关闭数据库连接, 设置为true无需使用using或者Close操作
                InitKeyType = InitKeyType.SystemTable //默认SystemTable, 字段信息读取, 如：该属性是不是主键，是不是标识列等等信息
            });

            if (AppSettings.PrintSqlOpen)
            {
                // 调式代码 用来打印SQL
                DbSqlSugarClient.Aop.OnLogExecuting = (sql, pars) =>
                {
                    Console.WriteLine(sql + "\r\n" +
                                      DbSqlSugarClient.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                    Console.WriteLine();
                };
            }

        }

        /// <summary>
        /// DbSqlSugarClient
        /// </summary>
        public SqlSugarClient DbSqlSugarClient;

    }
}
