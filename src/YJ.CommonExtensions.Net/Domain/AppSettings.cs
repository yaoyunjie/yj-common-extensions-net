﻿#region 说明

/* 独取配置文件中文乱码问题
 * 解决：将配置文件另存为utf-8即可
 */

#endregion

using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace YJ.CommonExtensions.Net.Domain
{
    /// <summary>
    /// 读取appsettings.json配置文件
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// 配置文件的根节点
        /// </summary>
        protected static readonly IConfigurationRoot Config;

        /// <summary>
        /// Constructor
        /// </summary>
        static AppSettings()
        {
            // 加载appsettings.json，并构建IConfigurationRoot
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true);
            Config = builder.Build();
        }

        /// <summary>
        /// 当前环境
        /// Development|Production
        /// </summary>
        public static string CurrentEnv => Config["ServerInfo:CurrentEnv"];

        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public static string ConnectionString => Config[$"ConnectionStrings:{CurrentEnv}DbConn"];

        /// <summary>
        /// 是否开启打印Sql
        /// 谨慎使用，当操作量比较多时会影响性能
        /// </summary>
        public static bool PrintSqlOpen => Convert.ToBoolean(Config[$"ConnectionStrings:PrintSqlOpen"]);

        /// <summary>
        /// CRM连接字符串
        /// </summary>
        public static string CrmConnectionString => Config[$"ConnectionStrings:{CurrentEnv}CrmConn"];

        /// <summary>
        /// CRM用户
        /// </summary>
        public static string UserName => Config[$"ConnectionStrings:UserName"];

        /// <summary>
        /// CRM密码
        /// </summary>
        public static string Password => Config[$"ConnectionStrings:Password"];

        /// <summary>
        /// CRM域
        /// </summary>
        public static string Domain => Config[$"ConnectionStrings:Domain"];
        /// <summary>
        /// SMTP邮件服务器地址
        /// </summary>
        public static string SmtpServer => Config["EmailInfo:SmtpServer"];
        /// <summary>
        /// SMTP邮件服务器端口
        /// </summary>
        public static int SmtpPort => Convert.ToInt32(Config["EmailInfo:Port"]);
        /// <summary>
        /// 账户
        /// </summary>
        public static string SmtpAccount => Config["EmailInfo:Account"];
        /// <summary>
        /// 密码
        /// </summary>
        public static string SmtpPassword => Config["EmailInfo:Password"];
        /// <summary>
        /// 发件人名称
        /// </summary>
        public static string SenderName => Config["EmailInfo:SenderName"];
        /// <summary>
        /// 发件人地址
        /// </summary>
        public static string SenderAddress => Config["EmailInfo:SenderAddress"];
        /// <summary>
        /// 收件人，多个地址之间用";"隔开
        /// </summary>
        public static string Recipients => Config["EmailInfo:Recipients"];
        /// <summary>
        /// 错误警告邮件收件人，多个地址之间用";"隔开
        /// </summary>
        public static string WarningEmailRecipients => Config["EmailInfo:WarningEmailRecipients"];
        /// <summary>
        /// 邮件抄送人
        /// </summary>
        public static string Cc => Config["EmailInfo:CC"];
        /// <summary>
        /// 邮件主题
        /// </summary>
        public static string Subject => Config["EmailInfo:Subject"];
        /// <summary>
        /// 日志路径
        /// </summary>
        public static string LogPath => Config["Log:Path"];
        /// <summary>
        /// 共享文件存放目录
        /// </summary>
        public static string OutputFolder => Config["File:OutputFolder"];
        /// <summary>
        /// RedisConnectionString
        /// </summary>
        public static string RedisConnectionString => Config["Redis:ConnectionString"];
    }
}
