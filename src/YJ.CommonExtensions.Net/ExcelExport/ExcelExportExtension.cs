﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Magicodes.ExporterAndImporter.Core.Models;
using Magicodes.ExporterAndImporter.Excel;

namespace YJ.CommonExtensions.Net.ExcelExport
{
    /// <summary>
    /// Excel导出扩展
    /// 采用Magicodes.IE框架
    /// </summary>
    public static class ExcelExportExtension
    {
        /// <summary>
        /// 使用第三方库（Magicodes.IE）导出Excel
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="fileName">导出的文件名（需要包含路径信息）</param>
        /// <param name="data">待导出的数据列表</param>
        /// <returns></returns>
        public static async Task<ExportFileInfo> ExportExcelAsync<T>(string fileName, List<T> data)
            where T : class, new()
        {
            var exporter = new ExcelExporter();
            var result = await exporter.Export(fileName, data);
            return result;
        }
    }
}
