﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using YJ.CommonExtensions.Net.Domain;
using YJ.CommonExtensions.Net.Logger;

namespace YJ.CommonExtensions.Net.Mail
{
    /// <summary>
    /// 邮件发送扩展
    /// 采用Mailkit框架
    /// </summary>
    public static class MailKitExtension
    {
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="subject">主题</param>
        /// <param name="content">内容</param>
        /// <param name="emailType">邮件类型，正常||异常</param>
        /// <param name="recipients">收件人，多个地址用;隔开</param>
        /// <param name="sendToSpecify">收件人是否为配置项中指定的人员</param>
        public static async Task SendEmailAsync(string subject, string content, EmailTypeEnum emailType, string recipients, bool sendToSpecify = false)
        {
            Log.Info($"邮件信息：{subject},{content}");
            var sendMessage = new MimeMessage();
            sendMessage.Subject = subject;
            sendMessage.Sender = new MailboxAddress(AppSettings.SenderName, AppSettings.SenderAddress);
            // sendMessage.From.Add(new MailboxAddress("发件人", "发件人邮箱"));
            // sendMessage.From.AddRange(new List<InternetAddress>());
            // 收件人配置
            if (emailType == EmailTypeEnum.Normal)
            {
                if (sendToSpecify) recipients = AppSettings.Recipients;
                if (string.IsNullOrEmpty(recipients)) throw new ArgumentNullException("MailKitExtension，收件人信息不能为空！");
                var toList = recipients.Split(';').Select(s => new MailboxAddress("", s));
                sendMessage.To.AddRange(toList);
            }
            else
            {
                var warnRecipients = AppSettings.WarningEmailRecipients;
                if (string.IsNullOrEmpty(warnRecipients)) throw new ArgumentNullException("MailKitExtension，收件人信息不能为空！");
                var toList = warnRecipients.Split(';').Select(s => new MailboxAddress("", s));
                sendMessage.To.AddRange(toList);
                // 如果是异常通知邮件，可以指定抄送给特殊人员
                Log.Warn($"CC给指定人员：{AppSettings.Cc}");
                sendMessage.Cc.Add(new MailboxAddress("", AppSettings.Cc));
            }
            var body = new TextPart(TextFormat.Text) { Text = content };
            sendMessage.Body = body;

            using (var smtp = new MailKit.Net.Smtp.SmtpClient())
            {
                // 1. 注册邮件发送成功之后的事件，比如记录log
                // MessageSent事件里可以通过args参数，获得服务器的响应信息，以便于记录Log。
                smtp.MessageSent += (sender, args) =>
                {
                    Log.Info($"邮件发送结束，{args.Response}");
                };
                smtp.ServerCertificateValidationCallback = (s, c, h, e) => true;
                // 2. 连接服务器
                await smtp.ConnectAsync(AppSettings.SmtpServer, AppSettings.SmtpPort, SecureSocketOptions.StartTls);
                // 3. 验证账号
                // ECC的SMTP服务器不支持验证，所以不需要这一步
                // 否则会报错，The SMTP server does not support authentication.
                // await smtp.AuthenticateAsync(AppSettings.SmtpAccount, AppSettings.SmtpPassword);
                // 4. 发送邮件
                await smtp.SendAsync(sendMessage);
                // 5. 释放链接
                await smtp.DisconnectAsync(true);
            }
        }

        /// <summary>
        /// 发送邮件（包含附件）
        /// </summary>
        /// <param name="subject">主题</param>
        /// <param name="content">内容</param>
        /// <param name="fileName">附件名称</param>
        /// <param name="attachmentPath">附件路径</param>
        /// <param name="emailType">邮件类型，正常||异常</param>
        public static async Task SendEmailWithAttachmentAsync(string subject, string content, string fileName, string attachmentPath, EmailTypeEnum emailType)
        {
            Log.Info($"邮件信息：{subject},{content}");
            var sendMessage = new MimeMessage();
            sendMessage.Subject = subject;
            sendMessage.Sender = new MailboxAddress(AppSettings.SenderName, AppSettings.SenderAddress);
            // sendMessage.From.Add(new MailboxAddress("发件人", "发件人邮箱"));
            // sendMessage.From.AddRange(new List<InternetAddress>());
            // 收件人配置
            if (emailType == EmailTypeEnum.Normal)
            {
                var recipients = AppSettings.Recipients;
                if (string.IsNullOrEmpty(recipients)) throw new ArgumentNullException("MailKitExtension，收件人信息不能为空！");
                var toList = recipients.Split(';').Select(s => new MailboxAddress("", s));
                sendMessage.To.AddRange(toList);
            }
            else
            {
                var warnRecipients = AppSettings.WarningEmailRecipients;
                if (string.IsNullOrEmpty(warnRecipients)) throw new ArgumentNullException("MailKitExtension，收件人信息不能为空！");
                var toList = warnRecipients.Split(';').Select(s => new MailboxAddress("", s));
                sendMessage.To.AddRange(toList);
                // 如果是异常通知邮件，可以指定抄送给特殊人员
                Log.Warn($"CC给指定人员：{AppSettings.Cc}");
                sendMessage.Cc.Add(new MailboxAddress("", AppSettings.Cc));
            }

            // 正文
            var builder = new BodyBuilder();
            builder.TextBody = content;
            // 这种方式也能实现附件发送，但是附件名称会带上路径信息，不太友好
            // builder.Attachments.Add(attachmentPath);
            using (FileStream fileStream = new FileStream(attachmentPath, FileMode.Open))
            {
                builder.Attachments.Add(fileName, fileStream);//需带完整路径或者相对路径
            }
            sendMessage.Body = builder.ToMessageBody();

            using (var smtp = new MailKit.Net.Smtp.SmtpClient())
            {
                // 1. 注册邮件发送成功之后的事件，比如记录log
                // MessageSent事件里可以通过args参数，获得服务器的响应信息，以便于记录Log。
                smtp.MessageSent += (sender, args) =>
                {
                    Log.Info($"邮件发送结束，{args.Response}");
                };
                smtp.ServerCertificateValidationCallback = (s, c, h, e) => true;
                // 2. 连接服务器
                await smtp.ConnectAsync(AppSettings.SmtpServer, AppSettings.SmtpPort, SecureSocketOptions.StartTls);
                // 3. 验证账号
                // ECC的SMTP服务器不支持验证，所以不需要这一步
                // 否则会报错，The SMTP server does not support authentication.
                // await smtp.AuthenticateAsync(AppSettings.SmtpAccount, AppSettings.SmtpPassword);
                // 4. 发送邮件
                await smtp.SendAsync(sendMessage);
                // 5. 释放链接
                await smtp.DisconnectAsync(true);
            }
        }
    }
}
