﻿using System.ComponentModel;

namespace YJ.CommonExtensions.Net.Mail
{
    /// <summary>
    /// 邮件类型枚举类
    /// </summary>
    public enum EmailTypeEnum
    {
        /// <summary>
        /// 普通邮件
        /// </summary>
        [Description("正常邮件")]
        Normal = 1,
        /// <summary>
        /// 警报邮件
        /// </summary>
        [Description("异常邮件")]
        Warning = 2
    }
}
